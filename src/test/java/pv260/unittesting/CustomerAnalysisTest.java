package pv260.unittesting;

import static com.googlecode.catchexception.CatchException.catchException;
import java.io.IOException;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import pv260.unittesting.CustomerAnalysis.CantUnderstandException;
import pv260.unittesting.CustomerAnalysis.Customer;
import pv260.unittesting.CustomerAnalysis.ErrorHandler;
import pv260.unittesting.CustomerAnalysis.GeneralException;
import pv260.unittesting.CustomerAnalysis.NewsList;
import pv260.unittesting.CustomerAnalysis.Offer;
import pv260.unittesting.CustomerAnalysis.Product;
import pv260.unittesting.CustomerAnalysis.Storage;
import pv260.unittesting.CustomerAnalysis.AnalyticalEngine;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static com.googlecode.catchexception.CatchException.catchException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.AbstractList;
import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.logging.Level;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CustomerAnalysisTest {

    private static final String FAKE_QUERY = "GIMME_STUFF";

    private static final long FAKE_PROD_ID = 12345;

    @Mock
    private Product mockProduct;
    @Mock
    private AnalyticalEngine mockEngine;
    @Mock
    private CustomerAnalysis analysis;

    private List<AnalyticalEngine> supplier = new ArrayList<>();

    private List<Customer> customerList = new ArrayList<>();

    @Before
    public void setUp() {

        supplier.add(mockEngine);
        supplier.add(mockEngine);
        supplier.add(mockEngine);
        customerList.add(new Customer(FAKE_PROD_ID, "Customer1", 100));
        customerList.add(new Customer(FAKE_PROD_ID+1, "Customer2", 200));

    }

    /**
     * Verify the ErrorHandler is invoked when one of the AnalyticalEngine methods throws exception and the exception is not re-thrown from the CustomerAnalysis. The exception is passed to the ErrorHandler directly, not wrapped.
     */
    @Test(expected = GeneralException.class)
    public void testErrorHandlerInvokedWhenEngineThrows() throws GeneralException {
        Product dummyProduct = mock(Product.class);
        CustomerAnalysis custAnalysis = mock(CustomerAnalysis.class);
        when(custAnalysis.findInterestingCustomers(dummyProduct)).thenThrow(GeneralException.class);
        custAnalysis.findInterestingCustomers(dummyProduct);
        ErrorHandler errhandle = mock(ErrorHandler.class);
        verify(errhandle, times(1)).handle(isA(GeneralException.class));
    }

    /**
     * Verify that if first AnalyticalEngine fails by throwing an exception, subsequent engines are tried with the same input. Ordering of engines is given by their order in the List passed to constructor of AnalyticalEngine
     */
    @Test
    public void testSubsequentEnginesTriedIfOneFails() throws GeneralException {
        Product dummyProduct = mock(Product.class);
        AnalyticalEngine analyticEng = mock(AnalyticalEngine.class);
        //jaky je rozdil kdyz to haze exception tady
        // when(analyticEng.interesetingCustomers(dummyProduct)).thenThrow(GeneralException.class);
        when(analyticEng.interesetingCustomers(dummyProduct)).thenReturn(new LinkedList<>());
        List<AnalyticalEngine> supplier = new ArrayList<>();
        supplier.add(analyticEng);
        supplier.add(analyticEng);
        supplier.add(analyticEng);
        CustomerAnalysis custAnalysis = new CustomerAnalysis(supplier, mock(Storage.class), mock(NewsList.class), mock(ErrorHandler.class));

        // a kdyz se prida toto tak to vyhodi tez ServiceUnavailableException (jednu navic?)
        // proc vlastne toto projde zbytek kodu v findInterestingCustomers? viz ten funkcni error handler apod.
        if (true) {
            try {
                when(custAnalysis.findInterestingCustomers(dummyProduct)).thenThrow(GeneralException.class);
            } catch (CustomerAnalysis.ServiceUnavailableException serviceUnavailableException) {
                System.out.println("***WHEN Exception thrown***:" + serviceUnavailableException);
            }
        }

        try {
            custAnalysis.findInterestingCustomers(dummyProduct);
        } catch (CustomerAnalysis.ServiceUnavailableException serviceUnavailableException) {
            System.out.println("***Exception thrown***:" + serviceUnavailableException);
        }

        verify(analyticEng, times(3)).interesetingCustomers(dummyProduct);
    }

    @Test
    public void testSubsequentEnginesTriedIfOneFails1() throws GeneralException {

        Product product = new Product(FAKE_PROD_ID, "BZZZZ", FAKE_QUERY, 100);
        AnalyticalEngine engine = new AnalyticalEngine() {

            @Override
            public List<Customer> interesetingCustomers(Product product) throws GeneralException {
                System.out.println("Throwing General Exception");
                throw new GeneralException();
            }

            @Override
            public Map<String, Integer> uninterestingData() throws GeneralException {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        AnalyticalEngine spyEngine = Mockito.spy(engine);

    //   when se aplikuje na "engine", i kdyz to neni mock?    
        //    when(engine.interesetingCustomers(product)).thenThrow(GeneralException.class);
        List<AnalyticalEngine> list = new ArrayList<>();
        list.add(spyEngine);
        list.add(spyEngine);
        list.add(spyEngine);
        CustomerAnalysis analysis = new CustomerAnalysis(list, mock(Storage.class), mock(NewsList.class), mock(ErrorHandler.class));

        try {
            analysis.findInterestingCustomers(product);
        } catch (CustomerAnalysis.ServiceUnavailableException serviceUnavailableException) {
            System.out.println("Service unavailable Exception");

        }

        verify(spyEngine, times(3)).interesetingCustomers(product);
    }

    /**
     * Verify that as soon as the first AnalyticalEngine succeeds, this result is returned as result and no subsequent AnalyticalEngine is invoked for this input
     */
    @Test
    public void testNoMoreEnginesTriedAfterOneSucceeds1() throws GeneralException {
        //when(analysis.findInterestingCustomers(mockProduct)).thenReturn(customerList);
        //when(analysis.findInterestingCustomers(mockProduct)).thenCallRealMethod();

        //proc tady ta mocked analysis nedava zadny result? - protoze mock je jen fake class...?
        when(mockEngine.interesetingCustomers(mockProduct)).thenReturn(customerList);
        List<Customer> result = analysis.findInterestingCustomers(mockProduct);

        try {
            System.out.println(result.get(0));
        } catch (Exception e) {
            System.out.println("Why no result?");
        }

        analysis = new CustomerAnalysis(new ArrayList<>(), mock(Storage.class), mock(NewsList.class), mock(ErrorHandler.class));

        try {
            Field field = analysis.getClass().getDeclaredField("engines");
            field.setAccessible(true);
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            field.set(analysis, supplier);

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(CustomerAnalysisTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<Customer> result2 = analysis.findInterestingCustomers(mockProduct);
        verify(mockEngine, times(1)).interesetingCustomers(mockProduct);
        assertEquals(result2.get(0), customerList.get(0));

    }

    @Test
    public void testNoMoreEnginesTriedAfterOneSucceeds() throws GeneralException {
//        when(analysis.findInterestingCustomers(mockProduct)).thenReturn(any());
//        analysis.findInterestingCustomers(mockProduct);
//        verify(analysis, times(1)).findInterestingCustomers(mockProduct);

        analysis = new CustomerAnalysis(supplier, mock(Storage.class), mock(NewsList.class), mock(ErrorHandler.class));

        when(mockEngine.interesetingCustomers(mockProduct)).thenReturn(customerList);
        List<Customer> result = analysis.findInterestingCustomers(mockProduct);
        verify(mockEngine, times(1)).interesetingCustomers(mockProduct);
        assertEquals(result.get(0), customerList.get(0));
        Mockito.verifyNoMoreInteractions(mockEngine, mockProduct);

    }

    @Test
    public void todoVogella() throws GeneralException {
        Comparable c = mock(Comparable.class);
        when(c.compareTo(isA(Integer.class))).thenReturn(0);
        //assert
        Integer todo = new Integer(5);
        assertEquals(c.compareTo(todo), c.compareTo(new Integer(1)));
    }

    /**
     * Verify that once Offer is created for the Customer, this order is persisted in the Storage before being added to the NewsList HINT: you might use mockito InOrder
     */
    @Test
    public void testOfferIsPersistedBefreAddedToNewsList() throws GeneralException {
        
        Storage storage = mock(Storage.class);
        NewsList newsList = mock(NewsList.class);
        analysis = new CustomerAnalysis(supplier, storage, newsList, mock(ErrorHandler.class));


        when(storage.find(Product.class, FAKE_PROD_ID)).thenReturn(mockProduct);
    //    when(storage.persist(any(Offer.class))).thenReturn(Integer.BYTES);
    //    Mockito.doNothing().when(newsList).sendPeriodically(null);

        //how to capture Object creation/constructor?
        when(analysis.findInterestingCustomers(mockProduct)).thenReturn(customerList);
        analysis.calculateTotalCredit(FAKE_QUERY, FAKE_PROD_ID);
        
        //verify(newsList).sendPeriodically(any());
        
        //verify(analysis.findInterestingCustomers(mockProduct));
        
        ArgumentCaptor<Offer> argument1 = ArgumentCaptor.forClass(Offer.class);
        ArgumentCaptor<Offer> argument2 = ArgumentCaptor.forClass(Offer.class);
        InOrder inOrder = inOrder(storage, newsList);
        inOrder.verify(storage).persist(argument1.capture());
        inOrder.verify(newsList).sendPeriodically(argument2.capture());
        //inOrder.verify(newsList).sendPeriodically(any(Offer.class));
        assertEquals(argument1.getValue(), argument2.getValue());
    }

    /**
     * Verify that Offer is created for every selected Customer for the given Product test with at least two Customers selected by the AnalyticalEngine HINT: you might use mockito ArgumentCaptor
     */
    @Test
    public void testOfferContainsProductAndCustomer() throws GeneralException {        
        
        Storage storage = mock(Storage.class);
        analysis = new CustomerAnalysis(supplier, storage, mock(NewsList.class), mock(ErrorHandler.class));
         
        when(storage.find(Product.class, FAKE_PROD_ID)).thenReturn(mockProduct);
        when(analysis.findInterestingCustomers(mockProduct)).thenReturn(customerList);
        analysis.calculateTotalCredit(FAKE_QUERY, FAKE_PROD_ID);
        ArgumentCaptor<Offer> arguments = ArgumentCaptor.forClass(Offer.class);
//        ArgumentCaptor<?> argument1 = ArgumentCaptor.forClass(Customer.class);
//        ArgumentCaptor<?> argument2 = ArgumentCaptor.forClass(Product.class);
        //**verify(new Offer(any(), mockProduct)).getCustomer().getName();
        verify(storage,atLeast(1)).persist(arguments.capture());
        arguments.getAllValues().forEach((new Consumer<Object>() {

            public void accept(Object x) {
                Offer offer = (Offer) x;
                System.out.println(offer.getCustomer().getName());
            }
        }));
        
//        List <Offer> capturedOffers = (List <Offer>) arguments.getAllValues();
//        
//        for (Offer capture : capturedOffers) {
//            assertEquals(capture.getCustomer(), customerList.get(capturedOffers.indexOf(capture)));            
//        }
        
        arguments.getAllValues().forEach((capture) -> {
            assertEquals(customerList.get(arguments.getAllValues().indexOf(capture)),capture.getCustomer());
        });
        
         

    }

}
